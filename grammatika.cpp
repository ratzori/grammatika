#include "grammatika.h"
#include "ui_grammatika.h"
#include "keyenterfilter.h"

#include <QTextEdit>
#include <QFile>
#include <QStringList>
#include <QDebug>
#include <QEvent>
#include <QKeyEvent>
#include <QTimer>
#include <QRegularExpression>
#include <QTextToSpeech>
#include <QVoice>

Grammatika::Grammatika(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Grammatika)
{
    ui->setupUi(this);
    ui->answer->setVisible(false);
    ui->result->setVisible(false);

    KeyEnterFilter *filter = new KeyEnterFilter(this);
    ui->userInput->installEventFilter(filter);

    connect(filter, SIGNAL(enterPressed()),this, SLOT(enterPressed()));

    ui->hint->clear();
    ui->userInput->clear();
    ui->answer->clear();
    ui->result->clear();

    ui->userInput->setFocus();

    loadPhrases();
    pickNext();
    splitAnswerToWords();

    timer.setSingleShot(true);
    timer.stop();
    timer.setInterval(2000);

    connect(&timer, SIGNAL(timeout()), this, SLOT(pickNext()));

    initTextToSpeech();
}

Grammatika::~Grammatika()
{
    delete ui;
}

void Grammatika::initTextToSpeech(void)
{
    QStringList engines = QTextToSpeech::availableEngines();
    qDebug() << "QTextToSpeech: Available engines:";
    for (const auto &engine : engines) {
        qDebug() << "  " << engine;
    }
    mSpeech = new QTextToSpeech();
    qDebug() << "Available locales:";

    for (const auto &locale : mSpeech->availableLocales()) {
        qDebug() << "  " << locale;
    }

    mSpeech->setLocale(QLocale(QLocale::Russian, QLocale::CyrillicScript, QLocale::Russia));
    mSpeech->setRate(-0.4);
    mSpeech->setPitch(-0.2);
}

void Grammatika::loadPhrases(void)
{
    QFile file("fi-ru.txt");

    phrases.clear();

    if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
        QTextStream stream(&file);
        while (!stream.atEnd())
        {
            QString line = stream.readLine();
            phrases.append(line.split('|'));
        }
    }
    file.close();
}

void Grammatika::pickNext(void)
{
    if ( !phrases.empty() )
    {
        currentIndex++;

        if (currentIndex == phrases.count())
        {
            currentIndex = 0;
        }

        ui->hint->setText(phrases.at(currentIndex).at(0));
        ui->answer->setText(phrases.at(currentIndex).at(1));
        pronunciation = phrases.at(currentIndex).at(2);

        ui->userInput->clear();
    }
}

unsigned int Grammatika::CalcEditDistance(QString string1, QString string2)
{
    std::string s1 = string1.toStdString();
    std::string s2 = string2.toStdString();

    const std::size_t len1 = s1.size(), len2 = s2.size();
    std::vector<std::vector<unsigned int>> d(len1 + 1, std::vector<unsigned int>(len2 + 1));

    d[0][0] = 0;
    for(unsigned int i = 1; i <= len1; ++i) d[i][0] = i;
    for(unsigned int i = 1; i <= len2; ++i) d[0][i] = i;

    for(unsigned int i = 1; i <= len1; ++i)
        for(unsigned int j = 1; j <= len2; ++j)
                      // note that std::min({arg1, arg2, arg3}) works only in C++11,
                      // for C++98 use std::min(std::min(arg1, arg2), arg3)
                      d[i][j] = std::min({ d[i - 1][j] + 1, d[i][j - 1] + 1, d[i - 1][j - 1] + (s1[i - 1] == s2[j - 1] ? 0 : 1) });
    return d[len1][len2];
}

unsigned int Grammatika::CalcEditDistancePercent(QString string1, QString string2)
{
    int result = 100;

    int distance = CalcEditDistance(string1, string2);
    int stringLength = qMax(string1.length(),string2.length());
    stringLength = qMax(distance, stringLength);

    if ( stringLength > 0 )
    {
        result = (100 - ( (distance * 100) / stringLength));
    }

    return result;
}

void Grammatika::splitAnswerToWords(void)
{
    answerText = ui->answer->toPlainText().split(QRegularExpression("\\s+"));
}

void Grammatika::splitUserInputToWords(void)
{
    userInputText = ui->userInput->toPlainText().split(QRegularExpression("\\s+"));
}

void Grammatika::remapWords(void)
{
    wordMapping.clear();
    QVector<int> minDistances;

    for(int i = 0; i < userInputText.count(); i++)
    {
        int minDistance = INT_MAX;
        int minDistanceIndex = -1;

        QString inputWord = userInputText.at(i);
        QString mostSimilar = "";

        for(int j = 0; j < answerText.count(); j++)
        {
            QString answerWord = answerText.at(j);
            int distance = CalcEditDistance(inputWord, answerWord);

            if (distance < minDistance)
            {
                minDistance = distance;
                minDistanceIndex = j;
                mostSimilar = answerWord;
            }
        }

        if(minDistanceIndex == -1)
        {
            minDistanceIndex = i;
        }

        qDebug() << "Input word" << inputWord << "in index:" << i << "most similar with" << mostSimilar << "in index:" << minDistanceIndex << "distance" << minDistance;

        wordMapping.append(minDistanceIndex);
        minDistances.append(minDistance);
    }

    if ( userInputText.count() > answerText.count() )
    {
        int highestDistance = -1;
        int highestDistanceIndex = -1;

        int validWordCount = userInputText.count();

        while(validWordCount > answerText.count())
        {
            highestDistanceIndex = 0;
            highestDistance = -1;

            for (int i = 0; i < wordMapping.count();i++)
            {
                if ((wordMapping.at(i) != -1 ) &&
                     (minDistances.at(i) > highestDistance))
                {
                    highestDistance = minDistances.at(i);
                    highestDistanceIndex = i;
                }
            }

            wordMapping.replace(highestDistanceIndex, -1);

            validWordCount = 0;

            for (int i = 0; i < wordMapping.count(); i++)
            {
                if(wordMapping.at(i) != -1)
                {
                    validWordCount++;
                }
            }
        }

        qDebug() << "Wordmapping" << wordMapping;
    }
}

int Grammatika::replaceFirstOccurrence(QString &word, QChar charToFind, Qt::CaseSensitivity &caseSensitivy)
{
    caseSensitivy = Qt::CaseSensitive;

    int indexOfChar = word.indexOf(charToFind, 0, Qt::CaseInsensitive);

    if (indexOfChar != -1)
    {
        if(charToFind != word.at(indexOfChar))
        {
            caseSensitivy = Qt::CaseInsensitive;
        }

        word.replace(indexOfChar,1,'_');
        return indexOfChar;
    }

    return indexOfChar;
}

void Grammatika::on_userInput_textChanged()
{
    ui->result->clear();

    splitAnswerToWords();
    splitUserInputToWords();
    remapWords();

    QTextCursor cursor( ui->result->textCursor() );
    QTextCharFormat format;

    int word_count_max = qMax(userInputText.count(), answerText.count( ));

    for(int i = 0; i < word_count_max; i++)
    {
        ui->result->setTextColor(QColor("red"));

        if ( i < userInputText.count() )
        {
            //qDebug() << "Word";
            QString inputWord = userInputText.at(i);
            QString answerWord = "";
            QString tempAnswerWord;

            if (wordMapping.at(i) != -1)
            {
                answerWord = answerText.at(wordMapping.at(i));
                tempAnswerWord = answerWord;
            }

            int char_count_max = qMax(inputWord.count(), answerWord.count());
            int expectedCharIndex = 0;

            for(int j = 0; j < char_count_max; j++)
            {
                if ( j < inputWord.count() )
                {
                    Qt::CaseSensitivity caseSensitivy;

                    format.setFontStrikeOut(false);

                    int charIndex = replaceFirstOccurrence(tempAnswerWord, inputWord.at(j), caseSensitivy);

                    if (expectedCharIndex == charIndex)
                    {
                        if (caseSensitivy == Qt::CaseSensitive)
                        {
                            format.setForeground( QBrush( QColor( "green" ) ) );
                        }
                        else
                        {
                            format.setForeground( QBrush( QColor( "orange" ) ) );
                        }

                        expectedCharIndex++;
                    }
                    else if (charIndex != -1)
                    {
                        format.setForeground( QBrush( QColor( "red" ) ) );
                    }
                    else
                    {
                        format.setForeground( QBrush( QColor( "red" ) ) );
                        format.setFontStrikeOut(true);
                    }

                    cursor.setCharFormat( format );
                    cursor.insertText(inputWord.at(j));
                }
                else
                {
                    /* Characters missing from input word */
                    format.setForeground( QBrush( QColor( "red" ) ) );
                    cursor.setCharFormat( format );
                    cursor.insertText("_");
                }
            }

            format.setFontStrikeOut(false);
            cursor.setCharFormat( format );
            cursor.insertText(" ");
        }
        else if ( i < answerText.count( ) )
        {
            //qDebug() << "Word missing from user";

            QString missingWord;
            missingWord.fill('_', answerText.at(i).count());

            format.setForeground( QBrush( QColor( "red" ) ) );
            format.setFontStrikeOut(false);
            cursor.setCharFormat( format );
            cursor.insertText(missingWord);
            cursor.insertText(" ");
        }
        else
        {
            //qDebug() << "Extra word from user";
            format.setFontStrikeOut(true);
            format.setForeground( QBrush( QColor( "red" ) ) );
            cursor.setCharFormat( format );
            cursor.insertText(userInputText.at(i));
            cursor.insertText(" ");
        }
    }
}

void Grammatika::checkResult(void)
{
    int editDistancePercent =
            CalcEditDistancePercent(ui->answer->toPlainText(), ui->userInput->toPlainText());
    ui->editDistancePercentSpinBox->setValue(editDistancePercent);

    if ( editDistancePercent == 100 )
    {
        ui->result->append(pronunciation);
        mSpeech->say(ui->answer->toPlainText());
        timer.start();
    }

    ui->userInput->setHtml(ui->result->toHtml());
}

void Grammatika::enterPressed(void)
{
    checkResult();

    QTextCursor cursor( ui->userInput->textCursor() );
    cursor.movePosition(QTextCursor::End);

    QTextCharFormat format;
    format.setForeground( QBrush( QColor( "black" ) ) );
    format.setFontStrikeOut(false);

    cursor.setCharFormat(format);
}
