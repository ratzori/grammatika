#include "keyenterfilter.h"
#include <QEvent>
#include <QKeyEvent>

KeyEnterFilter::KeyEnterFilter(QObject *parent) : QObject(parent)
{

}

bool KeyEnterFilter::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::KeyPress)
    {
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);

        if ((keyEvent->key() == Qt::Key_Enter ||
                keyEvent->key() == Qt::Key_Return))
        {
            emit enterPressed();
            return true;
        }
    }
    // standard event processing
    return QObject::eventFilter(obj, event);
}
