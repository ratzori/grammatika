#ifndef GRAMMATIKA_H
#define GRAMMATIKA_H

#include <QMainWindow>
#include <QTimer>
#include <QTextToSpeech>

namespace Ui {
class Grammatika;
}

class Grammatika : public QMainWindow
{
    Q_OBJECT

public:
    explicit Grammatika(QWidget *parent = 0);
    ~Grammatika();

private slots:
    void on_userInput_textChanged();

    void pickNext(void);

    void enterPressed(void);

private:
    Ui::Grammatika *ui;

    void initTextToSpeech(void);
    void loadPhrases(void);
    unsigned int CalcEditDistance(QString string1, QString string2);
    unsigned int CalcEditDistancePercent(QString string1, QString string2);
    void splitAnswerToWords(void);
    void splitUserInputToWords(void);
    void remapWords(void);
    int replaceFirstOccurrence(QString &word, QChar charToFind, Qt::CaseSensitivity &caseSensitivy);
    void checkResult(void);

    QStringList answerText;
    QStringList userInputText;
    QString pronunciation;
    QVector<int> wordMapping;
    QVector<QStringList> phrases;
    int currentIndex = -1;
    QTimer timer;
    QTextToSpeech *mSpeech;
};

#endif // GRAMMATIKA_H
