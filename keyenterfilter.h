#ifndef KEYENTERFILTER_H
#define KEYENTERFILTER_H

#include <QObject>

class KeyEnterFilter : public QObject
{
    Q_OBJECT
public:
    explicit KeyEnterFilter(QObject *parent = nullptr);

signals:
    void enterPressed(void);

public slots:

protected:
    bool eventFilter(QObject *obj, QEvent *event);
};

#endif // KEYENTERFILTER_H
